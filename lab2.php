<?php 
/*menu items*/
$a = "salad";
$b = "hamburger";
$c = "cake";
/*food prices*/
$saladprice = 5;
$burgerprice = 6;
$cakeprice = 7;
/*tax and tip*/
$tax = 0.08;
$tip = 0.15;
$subtotal = $saladprice+$burgerprice+$cakeprice;
/*display prices and totals */
echo $a."- $".$saladprice." <br>".$b."- $".$burgerprice."<br> ".$c."- $".$cakeprice."<br>";
/* display subtotal */
echo "subtotal: $".$subtotal."<br> tax: $".$subtotal*$tax."<br> tip: $".$subtotal*$tip."<br>";
/* display grand total */
echo "grand total: $".$subtotal*(1+$tax+$tip);


?>